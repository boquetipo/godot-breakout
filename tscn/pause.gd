extends Node

func _on_continue_button_pressed():
	Events.set_pause(false)
	Events.emit_signal("change_pause")

func _on_quit_button_pressed():
	get_tree().quit()
