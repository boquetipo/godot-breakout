extends HSplitContainer

func _ready():
	if Events.connect("update_lives", self, "_on_update_lives_signal", [], CONNECT_DEFERRED) != OK:
		print("Unexpected error on connecting to update_lives signal")
	else:
		print("OK");

func _on_update_lives_signal(amount:int):
	print("enters")
	Variables.add_life(amount)
	$count.text = str(Variables.lives)
