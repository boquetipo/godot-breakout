extends Node2D

func _ready():
	if Events.connect("change_pause", self, "_on_change_pause_signal", [], CONNECT_DEFERRED) != OK:
		print("Unexpected error on connecting to change_pause signal")
	
	if Events.connect("change_stage", self, "_on_change_stage_signal", [], CONNECT_DEFERRED) != OK:
		print("Unexpected error on connecting to change_stage signal")
		
	$hub_upper/count.text = str(Variables.get_lives())
	
	go_to_next_stage()
	
func go_to_next_stage():
	var path = "stage_" + str(Variables.get_current_stage())
	
	if (has_node(path)):
		call_deferred("remove_child", get_node(path))
	
	Variables.next_stage()
	
	if (File.new().file_exists("res://tscn/stage_" + str(Variables.get_current_stage()) + ".tscn")):
		change_to_current_stage()
	else:
		if get_tree().change_scene("res://tscn/win.tscn") != OK:
			print("Unexpected error on change_scene to win.tscn")

func go_to_previous_stage():
	Variables.previous_stage();
	change_to_current_stage()
	
	var path = "stage_" + str(Variables.get_current_stage())
	
	if (has_node(path)):
		call_deferred("remove_child", get_node(path))
	
	Variables.previous_stage()
	
	if (File.new().file_exists("res://tscn/stage_" + str(Variables.get_current_stage()) + ".tscn")):
		change_to_current_stage()
	else:
		print("Stage " + str(Variables.get_current_stage()) + " does not exist");
	
func change_to_current_stage():
	var scene = load("res://tscn/stage_" + str(Variables.get_current_stage()) + ".tscn").instance()
	call_deferred("add_child", scene)
	call_deferred("move_child", scene, 0)

func _on_Timer_timeout():
	go_to_next_stage()
	
func _on_change_stage_signal():
	go_to_next_stage()

func _on_change_pause_signal():
	var path = "pause"
			
	match Events.is_paused():
		true:
			var scene = load("res://tscn/" + path + ".tscn").instance()
			call_deferred("add_child", scene)
		false:
			if (has_node(path)):
				call_deferred("remove_child", get_node(path))
