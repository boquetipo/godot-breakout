extends Node

signal change_stage
signal change_pause
signal update_lives

var playing:bool = false

func is_playing() -> bool:
	return playing

func set_is_playing(state:bool) -> void:
	playing = state

func is_paused() -> bool:
	return get_tree().paused

func set_pause(state:bool) -> void:
	get_tree().paused = state
