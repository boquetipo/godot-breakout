extends Node

func _on_start_button_pressed():
	if get_tree().change_scene("res://tscn/world.tscn") != OK:
		print("Unexpected error on change_scene to world.tscn")

func _on_quit_button_pressed():
	get_tree().quit()
