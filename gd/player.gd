extends KinematicBody2D

onready var replay: PackedScene = preload("res://tscn/pause.tscn")

export var speed: int = 120

func get_input(_delta):
	if Input.is_action_pressed("move_left"):
		move_and_collide(Vector2(-speed * _delta, 0))
	if Input.is_action_pressed("move_right"):
		move_and_collide(Vector2(speed * _delta, 0))
	if Input.is_action_just_released("pause"):
		Events.set_pause(true)
		Events.emit_signal("change_pause")
		
func _physics_process(_delta):
	get_input(_delta)
