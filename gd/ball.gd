extends RigidBody2D

var is_dead: bool = false
var bodies: Array

func _input(event):
	if !Events.is_playing() && !is_dead && Variables.get_lives() > 0 && event.is_action_pressed("start"):
		$trail.emitting = true
#		sleeping = false # it does not affect. I do not know why
		Events.set_is_playing(true)
		linear_velocity = Vector2(80, 300)

func _process(delta):
	if Variables.get_lives() == 0:
		Variables.add_life(3)
		get_tree().change_scene("res://tscn/title.tscn")

func _physics_process(_delta):
	for body in get_colliding_bodies():
		if body.is_in_group("gr_blocks"):
			$hit.play()
			body.queue_free();
			
			match body.get_parent().get_child_count():
				8:
					Events.emit_signal("change_stage")
	#				get_parent().go_to_next_stage() # also works, if parent is world
				0:
					Events.emit_signal("change_stage")

func _integrate_forces(state):
	if (is_dead):
		reset_state(state)
		is_dead = false
		$timer_die.start()

func reset_state(state:Physics2DDirectBodyState):
#	state.transform = Transform2D(0.0, Vector2(80, 300)) # also works
	state.set_transform(Transform2D(0.0, Vector2(80, 300)))
	state.linear_velocity = Vector2(0, 0)

func _on_ball_body_entered(body) -> void:
	if (body.get_name() == "stbd_border_bottom"):
		trigger_out_of_bounds()
		
func _on_out_of_bounds_screen_exited():
	## since exiting calculations are not accurate due to performance,
	## ball position has to be further than expected
	trigger_out_of_bounds()

func trigger_out_of_bounds() -> void:
	hide()
	$trail.emitting = false
	Events.emit_signal("update_lives", -1)
	Events.set_is_playing(false)
	is_dead = true

func _on_timer_die_timeout() -> void:
#	sleeping = true
	$timer_die.stop()
	show()
