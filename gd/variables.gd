extends Node

var lives: int = 3
var points: int = 0
var current_stage = 0

func get_lives() -> int:
	return lives

func add_life(amount:int) -> void:
	lives += amount;

func get_points() -> int:
	return points

func add_points(amount:int) -> void:
	points += amount

func get_current_stage() -> int:
	return current_stage

func set_current_stage(stage:int) -> void:
	current_stage = stage

func next_stage() -> void:
	set_current_stage(current_stage + 1)

func previous_stage() -> void:
	set_current_stage(current_stage - 1)
